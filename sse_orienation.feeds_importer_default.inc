<?php
/**
 * @file
 * sse_orienation.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function sse_orienation_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'programs_orientation';
  $feeds_importer->config = array(
    'name' => 'Programs',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'encoding' => 'UTF-8',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 1,
        'mappings' => array(
          0 => array(
            'source' => 'Code',
            'target' => 'field_program_code_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          1 => array(
            'source' => 'Program Name',
            'target' => 'title',
            'unique' => FALSE,
            'language' => 'und',
          ),
          2 => array(
            'source' => 'Orientation Day',
            'target' => 'field_date_string_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          3 => array(
            'source' => 'Orientation Time',
            'target' => 'field_time_string_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          4 => array(
            'source' => 'Orientation Location',
            'target' => 'field_school_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          5 => array(
            'source' => 'School',
            'target' => 'field_school_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          6 => array(
            'source' => 'Campus',
            'target' => 'field_campus_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          7 => array(
            'source' => 'Additional Information',
            'target' => 'field_info_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          8 => array(
            'source' => 'Additional Information',
            'target' => 'field_address_string_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          9 => array(
            'source' => 'Blank',
            'target' => 'field_date_orientation:start',
            'unique' => FALSE,
            'language' => 'und',
          ),
          10 => array(
            'source' => 'GUID',
            'target' => 'guid',
            'unique' => 1,
          ),
          11 => array(
            'source' => 'Address',
            'target' => 'field_address_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          12 => array(
            'source' => 'Address',
            'target' => 'field_address_string_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
          13 => array(
            'source' => 'Orientation Location',
            'target' => 'field_room_orientation',
            'unique' => FALSE,
            'language' => 'und',
          ),
        ),
        'insert_new' => '1',
        'update_existing' => '2',
        'update_non_existent' => 'unpublish',
        'input_format' => 'basic_html_sse',
        'skip_hash_check' => 0,
        'bundle' => 'program_orientation',
        'language' => 'und',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['programs_orientation'] = $feeds_importer;

  return $export;
}
