<?php
/**
 * @file
 * sse_orienation.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sse_orienation_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|entityform|register_orientation|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'register_orientation';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your Orientation Details',
    'weight' => '6',
    'children' => array(
      0 => 'group_program',
      1 => 'group_location',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Your Orientation Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-details field-group-fieldset',
        'description' => 'This is your Orientation information. We will be sending this information to the email you provided above.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_details|entityform|register_orientation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location|entityform|register_orientation|form';
  $field_group->group_name = 'group_location';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'register_orientation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_details';
  $field_group->data = array(
    'label' => 'Your Orientation Location & Time',
    'weight' => '19',
    'children' => array(
      0 => 'field_address_string_orientation',
      1 => 'field_campus_string_orientation',
      2 => 'field_date_string_orientation',
      3 => 'field_room_orientation',
      4 => 'field_time_string_orientation',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Your Orientation Location & Time',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-location field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_location|entityform|register_orientation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_program|entityform|register_orientation|form';
  $field_group->group_name = 'group_program';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'register_orientation';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_details';
  $field_group->data = array(
    'label' => 'Your Program',
    'weight' => '15',
    'children' => array(
      0 => 'field_program_code_orientation',
      1 => 'field_program_name_orientation',
      2 => 'field_school_string_orientation',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-program field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_program|entityform|register_orientation|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ref_1_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_ref_1_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_section_f_orientation';
  $field_group->data = array(
    'label' => 'Reference 1: Faculty/Staff',
    'weight' => '21',
    'children' => array(
      0 => 'field_email_ref_a_orientation',
      1 => 'field_name_ref_a_orientation',
      2 => 'field_org_ref_a_orientation',
      3 => 'field_phone_ref_a_orientation',
      4 => 'field_pos_ref_a_orientation',
      5 => 'field_rel_ref_a_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-ref-1-orientation field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_ref_1_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ref_2_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_ref_2_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_section_f_orientation';
  $field_group->data = array(
    'label' => 'Reference 2',
    'weight' => '22',
    'children' => array(
      0 => 'field_email_ref_b_orientation',
      1 => 'field_name_ref_b_orientation',
      2 => 'field_org_ref_b_orientation',
      3 => 'field_phone_ref_b_orientation',
      4 => 'field_pos_ref_b_orientation',
      5 => 'field_rel_ref_b_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-ref-2-orientation field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_ref_2_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sectin_g_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_sectin_g_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section G: Completion of Application',
    'weight' => '31',
    'children' => array(
      0 => 'field_ack_vol_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-sectin-g-orientation field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_sectin_g_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_a_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_section_a_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section A: Applicant Information',
    'weight' => '0',
    'children' => array(
      0 => 'field_campus_orientation',
      1 => 'field_email_address_orientation',
      2 => 'field_first_name_orientation',
      3 => 'field_last_name_orientation',
      4 => 'field_phone_number_orientation',
      5 => 'field_preferred_name_orientation',
      6 => 'field_program_name_orientation',
      7 => 'field_school_orientation',
      8 => 'field_semester_orientation',
      9 => 'field_student_number_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section A: Applicant Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-section-a-orientation field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_section_a_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_b_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_section_b_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section B: Promotions',
    'weight' => '1',
    'children' => array(
      0 => 'field_other_source_orientation',
      1 => 'field_promo_source_orientation',
      2 => 'field_referral_name_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section B: Promotions',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-section-b-orientation field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_section_b_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_c_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_section_c_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section C: Residence',
    'weight' => '2',
    'children' => array(
      0 => 'field_liason_orientation',
      1 => 'field_living_orientation',
      2 => 'field_move_in_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section C: Residence',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-section-c-orientation field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_section_c_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_d_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_section_d_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section D: Motivation Questionnaire',
    'weight' => '3',
    'children' => array(
      0 => 'field_experience_orientation',
      1 => 'field_motivation_orientation',
      2 => 'field_other_motiv_orientati',
      3 => 'field_qualities_orientation',
      4 => 'field_roles_orientation',
      5 => 'field_three_words_orientation',
      6 => 'field_volunteered_cm_orientation',
      7 => 'field_volunteered_orientation',
      8 => 'field_volunteered_sm_orientation',
      9 => 'field_volunteered_yr_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section D: Motivation Questionnaire',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-section-d-orientation field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_section_d_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_e_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_section_e_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section E: Resume',
    'weight' => '4',
    'children' => array(
      0 => 'field_resume_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Section E: Resume',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-section-e-orientation field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_section_e_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_section_f_orientation|entityform|volunteer_application_orientatio|form';
  $field_group->group_name = 'group_section_f_orientation';
  $field_group->entity_type = 'entityform';
  $field_group->bundle = 'volunteer_application_orientatio';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Section F: References',
    'weight' => '5',
    'children' => array(
      0 => 'group_ref_1_orientation',
      1 => 'group_ref_2_orientation',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => 'group-section-f-orientation field-group-div',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_section_f_orientation|entityform|volunteer_application_orientatio|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Reference 1: Faculty/Staff');
  t('Reference 2');
  t('Section A: Applicant Information');
  t('Section B: Promotions');
  t('Section C: Residence');
  t('Section D: Motivation Questionnaire');
  t('Section E: Resume');
  t('Section F: References');
  t('Section G: Completion of Application');
  t('Your Orientation Details');
  t('Your Orientation Location & Time');
  t('Your Program');

  return $field_groups;
}
