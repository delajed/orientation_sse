<?php
/**
 * @file
 * sse_orienation.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function sse_orienation_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer feeds'.
  $permissions['administer feeds'] = array(
    'name' => 'administer feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'administer feeds_tamper'.
  $permissions['administer feeds_tamper'] = array(
    'name' => 'administer feeds_tamper',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'clear node feeds'.
  $permissions['clear node feeds'] = array(
    'name' => 'clear node feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'clear programs_orientation feeds'.
  $permissions['clear programs_orientation feeds'] = array(
    'name' => 'clear programs_orientation feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'clear user feeds'.
  $permissions['clear user feeds'] = array(
    'name' => 'clear user feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'create program_orientation content'.
  $permissions['create program_orientation content'] = array(
    'name' => 'create program_orientation content',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any program_orientation content'.
  $permissions['delete any program_orientation content'] = array(
    'name' => 'delete any program_orientation content',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own program_orientation content'.
  $permissions['delete own program_orientation content'] = array(
    'name' => 'delete own program_orientation content',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in campus_orientation'.
  $permissions['delete terms in campus_orientation'] = array(
    'name' => 'delete terms in campus_orientation',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in school_orientation'.
  $permissions['delete terms in school_orientation'] = array(
    'name' => 'delete terms in school_orientation',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any program_orientation content'.
  $permissions['edit any program_orientation content'] = array(
    'name' => 'edit any program_orientation content',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own program_orientation content'.
  $permissions['edit own program_orientation content'] = array(
    'name' => 'edit own program_orientation content',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in campus_orientation'.
  $permissions['edit terms in campus_orientation'] = array(
    'name' => 'edit terms in campus_orientation',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in school_orientation'.
  $permissions['edit terms in school_orientation'] = array(
    'name' => 'edit terms in school_orientation',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'import node feeds'.
  $permissions['import node feeds'] = array(
    'name' => 'import node feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'import programs_orientation feeds'.
  $permissions['import programs_orientation feeds'] = array(
    'name' => 'import programs_orientation feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'import user feeds'.
  $permissions['import user feeds'] = array(
    'name' => 'import user feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'publish button publish any program_orientation'.
  $permissions['publish button publish any program_orientation'] = array(
    'name' => 'publish button publish any program_orientation',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button publish own program_orientation'.
  $permissions['publish button publish own program_orientation'] = array(
    'name' => 'publish button publish own program_orientation',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish any program_orientation'.
  $permissions['publish button unpublish any program_orientation'] = array(
    'name' => 'publish button unpublish any program_orientation',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'publish button unpublish own program_orientation'.
  $permissions['publish button unpublish own program_orientation'] = array(
    'name' => 'publish button unpublish own program_orientation',
    'roles' => array(
      'editor' => 'editor',
      'owner' => 'owner',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: 'tamper node'.
  $permissions['tamper node'] = array(
    'name' => 'tamper node',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'tamper programs_orientation'.
  $permissions['tamper programs_orientation'] = array(
    'name' => 'tamper programs_orientation',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'tamper user'.
  $permissions['tamper user'] = array(
    'name' => 'tamper user',
    'roles' => array(),
    'module' => 'feeds_tamper',
  );

  // Exported permission: 'unlock node feeds'.
  $permissions['unlock node feeds'] = array(
    'name' => 'unlock node feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'unlock programs_orientation feeds'.
  $permissions['unlock programs_orientation feeds'] = array(
    'name' => 'unlock programs_orientation feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  // Exported permission: 'unlock user feeds'.
  $permissions['unlock user feeds'] = array(
    'name' => 'unlock user feeds',
    'roles' => array(),
    'module' => 'feeds',
  );

  return $permissions;
}
