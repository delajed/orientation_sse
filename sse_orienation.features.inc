<?php
/**
 * @file
 * sse_orienation.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sse_orienation_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function sse_orienation_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_default_entityform_type().
 */
function sse_orienation_default_entityform_type() {
  $items = array();
  $items['contact_us'] = entity_import('entityform_type', '{
    "type" : "contact_us",
    "label" : "Contact us",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "basic_html_sse" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "Message sent.",
      "submission_text" : {
        "value" : "\\u003Cp\\u003E\\u003Cbr \\/\\u003E\\r\\nThank you contacting us! We will be in touch soon.\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Ch3\\u003EHave you registered for Orientation yet? If you haven\\u0027t,\\u003Ca href=\\u0022schedule\\u0022\\u003E Let\\u0027s get Started!\\u003C\\/a\\u003E\\u003C\\/h3\\u003E\\r\\n",
        "format" : "basic_html_sse"
      },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "4" : "4", "3" : "3" },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Ch3\\u003EHave a question? We\\u0027ve got answers.\\u003C\\/h3\\u003E\\r\\n",
        "format" : "basic_html_sse"
      }
    },
    "weight" : "0",
    "rdf_mapping" : [],
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/contact-us",
        "alias" : "contact-us",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/contact-us\\/confirm",
        "alias" : "contact-us\\/sent",
        "language" : "und"
      }
    }
  }');
  $items['international_student_orientatio'] = entity_import('entityform_type', '{
    "type" : "international_student_orientatio",
    "label" : "International Student Orientation Registration",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "basic_html_sse" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "Your Registration is Complete.",
      "submission_text" : {
        "value" : "\\u003Cp\\u003EThank you for registering for the International Student Orientation.\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Cp\\u003EPlease make a note of the location and times:\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Cul\\u003E\\r\\n\\t\\u003Cli\\u003E\\u003Cstrong\\u003ENorth Campus\\u003C\\/strong\\u003E \\u0026amp; \\u003Cstrong\\u003ECarrier Drive\\u003C\\/strong\\u003E Students: Friday, Sept. 2nd, 10am, \\u003Ca href=\\u0022http:\\/\\/www.humber.ca\\/contact-us\\/maps\\u0022 target=\\u0022_blank\\u0022\\u003EMain Gym\\u003C\\/a\\u003E\\u003C\\/li\\u003E\\r\\n\\t\\u003Cli\\u003E\\u003Cstrong\\u003ELakeshore Campus\\u003C\\/strong\\u003E Students: Friday, Sept. 2nd, 10am , \\u003Ca href=\\u0022http:\\/\\/www.humber.ca\\/contact-us\\/maps\\u0022 target=\\u0022_blank\\u0022\\u003EBuilding A - Room A128 - Auditorium\\u003C\\/a\\u003E\\u003C\\/li\\u003E\\r\\n\\u003C\\/ul\\u003E\\r\\n",
        "format" : "basic_html_sse"
      },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "4" : "4", "3" : "3" },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : {
        "value" : "\\u003Cp\\u003EDuring Orientation week, the \\u003Ca href=\\u0022https:\\/\\/international.humber.ca\\/\\u0022 target=\\u0022_blank\\u0022\\u003EInternational Centre\\u003C\\/a\\u003E will be running a welcome session for all students who are studying from abroad.\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Cp\\u003ELearn about upcoming workshops, trips, advising, health insurance and immigration. \\u0026nbsp;\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Cp\\u003ELocation \\u0026amp; Times:\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Cul\\u003E\\r\\n\\t\\u003Cli\\u003E\\u003Cstrong\\u003ENorth Campus\\u003C\\/strong\\u003E \\u0026amp; \\u003Cstrong\\u003ECarrier Drive\\u003C\\/strong\\u003E Students: Friday, Sept. 2nd, 10am, \\u003Ca href=\\u0022http:\\/\\/www.humber.ca\\/contact-us\\/maps\\u0022 target=\\u0022_blank\\u0022\\u003EMain Gym\\u003C\\/a\\u003E\\u003C\\/li\\u003E\\r\\n\\t\\u003Cli\\u003E\\u003Cstrong\\u003ELakeshore Campus \\u003C\\/strong\\u003EStudents: Friday, Sept. 2nd, 10am , \\u003Ca href=\\u0022http:\\/\\/www.humber.ca\\/contact-us\\/maps\\u0022 target=\\u0022_blank\\u0022\\u003EBuilding A - Room A128 - Auditorium\\u003C\\/a\\u003E\\u003C\\/li\\u003E\\r\\n\\u003C\\/ul\\u003E\\r\\n\\r\\n\\u003Cp\\u003EPlease complete the following information to register for this event.\\u003C\\/p\\u003E\\r\\n",
        "format" : "basic_html_sse"
      }
    },
    "weight" : "0",
    "rdf_mapping" : [],
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/international-student-orientatio",
        "alias" : "orientation\\/start-at-humber\\/international-students\\/register",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/international-student-orientatio\\/confirm",
        "alias" : "orientation\\/start-at-humber\\/international-students\\/register\\/confirm",
        "language" : "und"
      }
    }
  }');
  $items['register_orientation'] = entity_import('entityform_type', '{
    "type" : "register_orientation",
    "label" : "Register",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "basic_html_sse" },
      "submit_button_text" : "Register",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "Thank you for registering!",
      "submission_text" : {
        "value" : "\\u003Cp\\u003EThank you for registering to attend your Orientation Day!\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Cp\\u003EWe have sent you a confirmation email with location and dates to reference.\\u003C\\/p\\u003E\\r\\n\\r\\n\\u003Ch3\\u003E\\u003Cstrong\\u003ENext step: \\u003C\\/strong\\u003EYour Orientation is only your first day! We have a \\u003Ca href=\\u0022node\\/100\\u0022 target=\\u0022_blank\\u0022\\u003Efull week of workshops\\u003C\\/a\\u003E and activities for you to join. \\u003Ca href=\\u0022node\\/100\\u0022 target=\\u0022_blank\\u0022\\u003EPrint the Schedule \\u00bb\\u003C\\/a\\u003E\\u003C\\/h3\\u003E\\r\\n",
        "format" : "basic_html_sse"
      },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "4" : "4", "3" : "3" },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : { "value" : "", "format" : "basic_html_sse" }
    },
    "weight" : "0",
    "rdf_mapping" : [],
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/register-orientation",
        "alias" : "register",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/register-orientation\\/confirm",
        "alias" : "register\\/confirmation",
        "language" : "und"
      }
    }
  }');
  $items['volunteer_application_orientatio'] = entity_import('entityform_type', '{
    "type" : "volunteer_application_orientatio",
    "label" : "Volunteer Application",
    "data" : {
      "draftable" : 0,
      "draft_redirect_path" : "",
      "draft_button_text" : "",
      "draft_save_text" : { "value" : "", "format" : "full_html_sse" },
      "submit_button_text" : "",
      "submit_confirm_msg" : "",
      "your_submissions" : "",
      "disallow_resubmit_msg" : "",
      "delete_confirm_msg" : "",
      "page_title_view" : "",
      "preview_page" : 0,
      "submission_page_title" : "Your application has been received.",
      "submission_text" : {
        "value" : "\\u003Cp\\u003EYou will be contacted shortly with regards to your application. Should you have any questions, feel free to contact us at orientation@humber.ca or connect with us on Twitter \\u003Ca href=\\u0022twitter.com\\/humberoteam\\u0022\\u003E@humberoteam\\u003C\\/a\\u003E.\\u003C\\/p\\u003E\\r\\n",
        "format" : "basic_html_sse"
      },
      "submission_show_submitted" : 0,
      "submissions_view" : "default",
      "user_submissions_view" : "default",
      "form_status" : "ENTITYFORM_OPEN",
      "roles" : { "1" : "1", "2" : "2", "4" : 0, "3" : 0 },
      "resubmit_action" : "new",
      "redirect_path" : "",
      "instruction_pre" : { "value" : "", "format" : "basic_html_sse" }
    },
    "weight" : "0",
    "rdf_mapping" : [],
    "paths" : {
      "submit" : {
        "source" : "eform\\/submit\\/volunteer-application-orientatio",
        "alias" : "join-our-team\\/apply",
        "language" : "und"
      },
      "confirm" : {
        "source" : "eform\\/volunteer-application-orientatio\\/confirm",
        "alias" : "join-our-team\\/apply\\/confirmation",
        "language" : "und"
      }
    }
  }');
  return $items;
}

/**
 * Implements hook_node_info().
 */
function sse_orienation_node_info() {
  $items = array(
    'program_orientation' => array(
      'name' => t('Program'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Program name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
