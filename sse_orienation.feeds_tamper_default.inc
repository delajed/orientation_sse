<?php
/**
 * @file
 * sse_orienation.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function sse_orienation_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'programs_orientation-address-rewrite';
  $feeds_tamper->importer = 'programs_orientation';
  $feeds_tamper->source = 'Address';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[address], Ontario, Canada',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['programs_orientation-address-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'programs_orientation-blank-rewrite';
  $feeds_tamper->importer = 'programs_orientation';
  $feeds_tamper->source = 'Blank';
  $feeds_tamper->plugin_id = 'rewrite';
  $feeds_tamper->settings = array(
    'text' => '[orientation day], [orientation time]',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Rewrite';
  $export['programs_orientation-blank-rewrite'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'programs_orientation-blank-strtotime';
  $feeds_tamper->importer = 'programs_orientation';
  $feeds_tamper->source = 'Blank';
  $feeds_tamper->plugin_id = 'strtotime';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'String to Unix timestamp';
  $export['programs_orientation-blank-strtotime'] = $feeds_tamper;

  return $export;
}
