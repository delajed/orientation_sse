<?php
/**
 * @file
 * sse_orienation.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function sse_orienation_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views--exp-program_orientation-sched'] = array(
    'cache' => -1,
    'css_class' => 'program-search',
    'custom' => 0,
    'delta' => '-exp-program_orientation-sched',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'sse_core' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sse_core',
        'weight' => 0,
      ),
      'sse_orientation' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sse_orientation',
        'weight' => -7,
      ),
    ),
    'title' => 'To register for Orientation you need to first find your program.',
    'visibility' => 0,
  );

  $export['views-program_orientation-loc'] = array(
    'cache' => -1,
    'css_class' => 'location-block',
    'custom' => 0,
    'delta' => 'program_orientation-loc',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'sse_core' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sse_core',
        'weight' => 0,
      ),
      'sse_orientation' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'sse_orientation',
        'weight' => -8,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
