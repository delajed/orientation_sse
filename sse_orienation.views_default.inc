<?php
/**
 * @file
 * sse_orienation.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sse_orienation_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'program_orientation';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Program';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Location';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '1';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'semanticviews_default';
  $handler->display->display_options['style_options']['group']['element_type'] = 'h2';
  $handler->display->display_options['style_options']['row']['element_type'] = '';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Field: Campus (field_campus_orientation) */
  $handler->display->display_options['relationships']['field_campus_orientation_tid']['id'] = 'field_campus_orientation_tid';
  $handler->display->display_options['relationships']['field_campus_orientation_tid']['table'] = 'field_data_field_campus_orientation';
  $handler->display->display_options['relationships']['field_campus_orientation_tid']['field'] = 'field_campus_orientation_tid';
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_address_orientation']['id'] = 'field_address_orientation';
  $handler->display->display_options['fields']['field_address_orientation']['table'] = 'field_data_field_address_orientation';
  $handler->display->display_options['fields']['field_address_orientation']['field'] = 'field_address_orientation';
  $handler->display->display_options['fields']['field_address_orientation']['label'] = '';
  $handler->display->display_options['fields']['field_address_orientation']['element_type'] = '0';
  $handler->display->display_options['fields']['field_address_orientation']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_address_orientation']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_address_orientation']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_address_orientation']['type'] = 'simple_gmap';
  $handler->display->display_options['fields']['field_address_orientation']['settings'] = array(
    'include_map' => 0,
    'include_static_map' => 1,
    'iframe_width' => '400',
    'iframe_height' => '225',
    'include_link' => 1,
    'link_text' => 'View on Google Maps',
    'zoom_level' => '15',
    'information_bubble' => 1,
    'include_text' => 0,
    'map_type' => 'm',
    'langcode' => 'en',
  );
  $handler->display->display_options['fields']['field_address_orientation']['field_api_classes'] = TRUE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_date_orientation']['id'] = 'field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation']['table'] = 'field_data_field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation']['field'] = 'field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation']['label'] = '';
  $handler->display->display_options['fields']['field_date_orientation']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_orientation']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_date_orientation']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_orientation']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_date_orientation']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_date_orientation']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  $handler->display->display_options['fields']['field_date_orientation']['field_api_classes'] = TRUE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_date_orientation_1']['id'] = 'field_date_orientation_1';
  $handler->display->display_options['fields']['field_date_orientation_1']['table'] = 'field_data_field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation_1']['field'] = 'field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation_1']['label'] = '';
  $handler->display->display_options['fields']['field_date_orientation_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_orientation_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_orientation_1']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Field: Campus */
  $handler->display->display_options['fields']['field_campus_orientation']['id'] = 'field_campus_orientation';
  $handler->display->display_options['fields']['field_campus_orientation']['table'] = 'field_data_field_campus_orientation';
  $handler->display->display_options['fields']['field_campus_orientation']['field'] = 'field_campus_orientation';
  $handler->display->display_options['fields']['field_campus_orientation']['label'] = '';
  $handler->display->display_options['fields']['field_campus_orientation']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_campus_orientation']['element_class'] = 'campus';
  $handler->display->display_options['fields']['field_campus_orientation']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_campus_orientation']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_campus_orientation']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_campus_orientation']['type'] = 'taxonomy_term_reference_plain';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program_orientation' => 'program_orientation',
  );

  /* Display: Location Block */
  $handler = $view->new_display('block', 'Location Block', 'loc');
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Address */
  $handler->display->display_options['fields']['field_address_orientation']['id'] = 'field_address_orientation';
  $handler->display->display_options['fields']['field_address_orientation']['table'] = 'field_data_field_address_orientation';
  $handler->display->display_options['fields']['field_address_orientation']['field'] = 'field_address_orientation';
  $handler->display->display_options['fields']['field_address_orientation']['label'] = '';
  $handler->display->display_options['fields']['field_address_orientation']['element_type'] = '0';
  $handler->display->display_options['fields']['field_address_orientation']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_address_orientation']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_address_orientation']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_address_orientation']['type'] = 'simple_gmap';
  $handler->display->display_options['fields']['field_address_orientation']['settings'] = array(
    'include_map' => 0,
    'include_static_map' => 1,
    'iframe_width' => '400',
    'iframe_height' => '225',
    'include_link' => 1,
    'link_text' => 'View on Google Maps',
    'zoom_level' => '15',
    'information_bubble' => 1,
    'include_text' => 0,
    'map_type' => 'm',
    'langcode' => 'en',
  );
  $handler->display->display_options['fields']['field_address_orientation']['field_api_classes'] = TRUE;
  /* Field: Taxonomy term: Address */
  $handler->display->display_options['fields']['field_address_campus_orientation']['id'] = 'field_address_campus_orientation';
  $handler->display->display_options['fields']['field_address_campus_orientation']['table'] = 'field_data_field_address_campus_orientation';
  $handler->display->display_options['fields']['field_address_campus_orientation']['field'] = 'field_address_campus_orientation';
  $handler->display->display_options['fields']['field_address_campus_orientation']['relationship'] = 'field_campus_orientation_tid';
  $handler->display->display_options['fields']['field_address_campus_orientation']['label'] = '';
  $handler->display->display_options['fields']['field_address_campus_orientation']['element_type'] = '0';
  $handler->display->display_options['fields']['field_address_campus_orientation']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_address_campus_orientation']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_address_campus_orientation']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_address_campus_orientation']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['field_address_campus_orientation']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  $handler->display->display_options['fields']['field_address_campus_orientation']['field_api_classes'] = TRUE;

  /* Display: Schedule */
  $handler = $view->new_display('page', 'Schedule', 'sched');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Register for Orientation';
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'basic_html_sse';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'semanticviews_default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_date_orientation',
      'rendered' => 1,
      'rendered_strip' => 1,
    ),
    1 => array(
      'field' => 'field_date_orientation_1',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['row']['class'] = 'schedule-item';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_date_orientation']['id'] = 'field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation']['table'] = 'field_data_field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation']['field'] = 'field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation']['label'] = '';
  $handler->display->display_options['fields']['field_date_orientation']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_orientation']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_date_orientation']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_orientation']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_date_orientation']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_date_orientation']['settings'] = array(
    'format_type' => 'month_day_year',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  $handler->display->display_options['fields']['field_date_orientation']['field_api_classes'] = TRUE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_date_orientation_1']['id'] = 'field_date_orientation_1';
  $handler->display->display_options['fields']['field_date_orientation_1']['table'] = 'field_data_field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation_1']['field'] = 'field_date_orientation';
  $handler->display->display_options['fields']['field_date_orientation_1']['label'] = '';
  $handler->display->display_options['fields']['field_date_orientation_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_date_orientation_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_date_orientation_1']['settings'] = array(
    'format_type' => 'time',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Field: Campus */
  $handler->display->display_options['fields']['field_campus_orientation']['id'] = 'field_campus_orientation';
  $handler->display->display_options['fields']['field_campus_orientation']['table'] = 'field_data_field_campus_orientation';
  $handler->display->display_options['fields']['field_campus_orientation']['field'] = 'field_campus_orientation';
  $handler->display->display_options['fields']['field_campus_orientation']['label'] = '';
  $handler->display->display_options['fields']['field_campus_orientation']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_campus_orientation']['element_class'] = 'campus';
  $handler->display->display_options['fields']['field_campus_orientation']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_campus_orientation']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_campus_orientation']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_campus_orientation']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_type'] = 'h4';
  $handler->display->display_options['fields']['title']['element_label_type'] = '0';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Date (field_date_orientation) */
  $handler->display->display_options['sorts']['field_date_orientation_value']['id'] = 'field_date_orientation_value';
  $handler->display->display_options['sorts']['field_date_orientation_value']['table'] = 'field_data_field_date_orientation';
  $handler->display->display_options['sorts']['field_date_orientation_value']['field'] = 'field_date_orientation_value';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'program_orientation' => 'program_orientation',
  );
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'word';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Program name';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
  );
  $handler->display->display_options['path'] = 'schedule';
  $export['program_orientation'] = $view;

  return $export;
}
